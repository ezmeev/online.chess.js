import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ApplicationTransportService } from '../services/application-transport.service';
import { HttpModule } from '@angular/http';
import { BoardService } from '../services/board.service';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        HttpModule,
        BrowserModule
    ],
    providers: [
        ApplicationTransportService,
        BoardService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
