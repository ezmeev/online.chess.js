import { Figure } from './figure.module';
import { Board } from './board.model';

export class GameState {
    public whiteSideSessionId: string;
    public blackSideSessionId: string;
    public selectedFigure: Figure;
    public sideToMove: string;
    public board: Board;
}
