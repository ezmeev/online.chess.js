import { BoardPositionModel } from './board-position.model';

export class Figure {
    public idx: number;
    public side: string;
    public type: string;
    public position: BoardPositionModel;
}
