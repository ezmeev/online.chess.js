import { Figure } from './figure.module';

export class Tile {
    public figure: Figure;
    public side: string;
    public availableForMove: boolean;
    public underAttack: boolean;
}
