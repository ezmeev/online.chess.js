import { Component, OnInit } from '@angular/core';
import { Figure } from './models/figure.module';
import { BoardService } from '../services/board.service';
import { GameState } from './models/game-state.model';
import { Http } from '@angular/http';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    public myName = '...';

    public gameState: GameState = {
        whiteSideSessionId: null,
        blackSideSessionId: null,
        selectedFigure: null,
        sideToMove: 'WHITE',
        board: {
            tiles: []
        }
    };

    constructor(private boardService: BoardService, private http: Http) {
    }

    public ngOnInit(): void {
        this.http.get('https://randomuser.me/api/').subscribe(user => {
            const username = user.json().results[0].login.username;
            const sessionId = username.substring(0, username.length - 3);

            this.boardService.init(sessionId, gameState => {
                this.gameState = gameState;
            });
            this.myName = this.boardService.getSessionId();
        });
    }

    public onFigureClick(figure: Figure): void {
        if (this.gameState.selectedFigure) {
            if (this.gameState.selectedFigure.side !== figure.side) {
                this.boardService.moveFigureTo(this.gameState.selectedFigure, figure.position.row, figure.position.col);
            } else {
                this.boardService.selectFigure(figure);
            }
        } else {
            this.boardService.selectFigure(figure);
        }
    }

    public isSelected(figure: Figure): boolean {
        return this.gameState.selectedFigure && this.gameState.selectedFigure.idx === figure.idx;
    }

    public figureName(figure: Figure) {
        return figure ? `${figure.side.slice(0, 1)}-${figure['type']}` : 'none';
    }

    public onTileClick(i: number, j: number) {
        if (this.gameState.selectedFigure) {
            this.boardService.moveFigureTo(this.gameState.selectedFigure, i, j);
        }
    }

    public getPlayerSummary(): string {
        let role = 'no side, just observer';
        if (this.gameState.whiteSideSessionId === this.myName) {
            role = 'WHITE';
        } else if (this.gameState.blackSideSessionId === this.myName) {
            role = 'BLACK';
        }
        return role;
    }

    public reset(): void {
        this.boardService.reset();
    }
}
