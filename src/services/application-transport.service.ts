import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { PlayerAction } from '../app/models/player-action.model';

@Injectable()
export class ApplicationTransportService {

    private sessionId: string;
    private socket: WebSocket;

    constructor(private http: Http) {
    }

    public initSession(sessionId: string) {
        this.sessionId = sessionId;

        this.socket = new WebSocket('ws://localhost:8081/');
        // this.socket = new WebSocket('ws://cda34fa6.ngrok.io/');
        this.socket.onopen = (e: Event) => {
            this.socket.send(this.sessionId);
        };
    }

    public send(action: PlayerAction) {
        this.socket.send(JSON.stringify(action));
    }

    public onServerUpdate(callback: Function) {
        this.socket.onmessage = m => {
            callback(JSON.parse(m.data));
        };
    }
}
