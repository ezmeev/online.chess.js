import { Injectable } from '@angular/core';
import { Figure } from '../app/models/figure.module';
import { ApplicationTransportService } from './application-transport.service';

@Injectable()
export class BoardService {

    private sessionId: string;

    constructor(private applicationTransportService: ApplicationTransportService) {
    }

    public init(sessionId: string, serverUpdateHandler: (data) => void) {
        this.sessionId = sessionId;
        this.applicationTransportService.initSession(sessionId);
        this.applicationTransportService.onServerUpdate(data => serverUpdateHandler(data));
    }

    public moveFigureTo(figure: Figure, row, col) {
        this.applicationTransportService.send({
            action: 'MoveFigure',
            payload: {
                figureIdx: figure.idx,
                fromPosition: {
                    row: figure.position.row,
                    col: figure.position.col
                },
                toPosition: {
                    row: row,
                    col: col
                }
            }
        });
    }

    public selectFigure(figure: Figure) {
        this.applicationTransportService.send({
            action: 'SelectFigure',
            payload: {
                figureIdx: figure.idx,
                position: {
                    row: figure.position.row,
                    col: figure.position.col
                }
            }
        });
    }

    public getSessionId(): string {
        return this.sessionId;
    }

    public reset() {
        this.applicationTransportService.send({
            action: 'Reset',
            payload: ''
        });
    }
}
